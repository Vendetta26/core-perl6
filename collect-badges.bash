#!/bin/bash

WRKDIR="./assets"
FNAMES=("license" "version" "commits" "issues" "release" "rakudo" "donate")
COLORS=("06c280" "blue" "yellow" "important" "f249ce" "blueviolet" "2118d6")
INDEX=0
ALLBDGS=

license=`head -n 1 LICENSE | sed 's/\s*\(.*\)\s*/\1/'`
version=`git log -1 | egrep -o -i -m 1 '([0-9]+\.[0-9]+\.[0-9]+)' | sed -r '/^\s*$/d'`
commits=`git rev-list --all --count`

# git tag --list --sort=creatordate | tail -n1
release=`curl -s https://gitlab.com/api/v4/projects/5110501/releases | jq '.[0] .name' | sed 's/"//g'`
rakudo=`raku --version | sed 's/.*\s\([0-9]\+.[0-9]\+\)\?.*/\1/' | sed -r '/^\s*$/d'`
issues=`curl -s https://gitlab.com/api/v4/projects/5110501/issues?state=opened\&per_page=100 | jq -r '. | length'`
donate="Paypal"

if [ ! -d "$WRKDIR" ]; then
    mkdir $WRKDIR
else
    rm -f $WRKDIR/*
fi

for NAME in "${FNAMES[@]}"
do
    echo -e "{\"schemaVersion\":1,\"label\":\"${NAME}\",\"message\":\"${!NAME}\",\"color\":\"${COLORS[${INDEX}]}\"}" > ${WRKDIR}/${NAME}-badge.json
    wget -q "https://img.shields.io/badge/${NAME}-${!NAME}-${COLORS[${INDEX}]}" -O ${WRKDIR}/${NAME}-badge.svg
    COMMA=
    if [ ! -z "$ALLBDGS" ]; then
        COMMA=", "
    fi
    ALLBDGS="${ALLBDGS}${COMMA}\"${NAME}\":\"${!NAME}\""
    INDEX=$((INDEX+1))
done

echo -e "{${ALLBDGS}, \"ci_job_id\":\"$CI_JOB_ID\"}" > ${WRKDIR}/all-in-one-badge.json

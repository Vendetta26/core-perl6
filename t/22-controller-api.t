use v6.d;
use Test;
use Test::Mock;
use lib 'lib';

use MIME::Base64;

use Pheix::Utils;
use Pheix::Test::FastCGI;
use Pheix::Controller::API;
use Pheix::Controller::Basic;

my $fcgi = Pheix::Test::FastCGI.new;
my $ctrl = Pheix::Controller::Basic.new(
    :apirobj(Nil),
    :mockedfcgi($fcgi),
    :test(True),
);
my $capi = $ctrl.sharedobj<ctrlapi>;

plan 6;

use-ok 'Pheix::Controller::API';

# Check error method
subtest {
    my @tokens;

    $ctrl.sharedobj<pageobj>.fill_seodata('404', 'Pheix');

    my %cntn = $capi.error(
        :route(q{/} ~ now.Rat ~ '/foo/bar'),
        :code('404'),
        :message('Page is not found'),
        :tick(1),
        :sharedobj($ctrl.sharedobj)
    );

    my Str $data = './conf/_pages/http-error.txt'.IO.slurp;

    $data ~~ m:g/ 'props.' (<[a..z_]>+) { @tokens.push($0) if $0 }/;

    # update content as: ./conf/_pages/convert-bytes-to-filechain.raku#119
    $data ~~ s:g/^^ <[\s]>+ //;
    $data ~~ s:g/^^ <[\s]>+ $$//;
    $data ~~ s:g/ <[\r\n]>+ //;

    plan 3 + @tokens.elems;

    ok (%cntn<component>:exists) && (%cntn<component> ne q{}), 'component';
    ok (%cntn<tparams>:exists) && (%cntn<tparams> ~~ Hash), 'tparams';

    is(
        $ctrl.sharedobj<mb64obj>.decode-str(%cntn<component>),
        $data,
        'component content'
    );

    for @tokens -> $t {
        ok (%cntn<tparams>{$t}:exists) && (%cntn<tparams>{$t}), $t;
    }
}, 'Check error method';

# Check index method
subtest {
    my @tokens;

    $ctrl.sharedobj<pageobj>.fill_seodata('index', 'Pheix');

    my %cntn = $capi.index(
        :route(q{/} ~ now.Rat ~ '/index'),
        :tick(1),
        :sharedobj($ctrl.sharedobj),
        :match(Hash.new)
    );

    my Str $data = './conf/_pages/index.txt'.IO.slurp;

    $data ~~ m:g/ 'props.' (<[a..z_]>+) { @tokens.push($0) if $0 }/;

    # update content as: ./conf/_pages/convert-to-filechain.bash#23
    $data ~~ s:g/^^ <[\s]>+ //;
    $data ~~ s:g/^^ <[\s]>+ $$//;
    $data ~~ s:g/ <[\r\n]>+ //;

    plan 4 + @tokens.elems;

    ok (%cntn<component>:exists) && (%cntn<component> ne q{}), 'component';
    ok (%cntn<component_render>:exists) && (%cntn<component> ne q{}), 'component_render';
    ok (%cntn<tparams>:exists) && (%cntn<tparams> ~~ Hash), 'tparams';
    is(
        $ctrl.sharedobj<mb64obj>.decode-str(%cntn<component>),
        $data,
        'component content'
    );

    for @tokens -> $t {
        ok (%cntn<tparams>{$t}:exists) && (%cntn<tparams>{$t}), $t;
    }
}, 'Check index method';

# Check captcha method - successful run
subtest {
    plan 4;

    my Str  $encap;
    my UInt $md = 64;

    my $cqry = $ctrl.sharedobj<utilobj>.do_encrypt('0123456789');

    todo 'possibly unpatched MagickWand';
    lives-ok {
        $encap = $capi.captcha(
            :route('/captcha/?' ~ $cqry),
            :tick(1),
            :sharedobj($ctrl.sharedobj),
            :match({query => q{?} ~ $cqry})
        );
    }, 'captcha method lives ok';

    if $encap {
        $encap ~~ s/'data:image/png;base64,'//;

        my $captchabuf = MIME::Base64.decode($encap);

        ok $captchabuf.bytes, 'captcha ' ~ $captchabuf.bytes ~ ' bytes';

        my $fh = './t/captchas/test-captcha.png'.IO.open;
        my $etalonbuf = $fh.read;
        $fh.close;

        my $delta =
            max($captchabuf.bytes, $etalonbuf.bytes) -
            min($captchabuf.bytes, $etalonbuf.bytes);

        todo 'requires ImageMagick 6.9.9-51 installed';
        ok $delta < $md, 'captcha delta ' ~ $delta ~ ' bytes';

        my Str $fpath = './t/captchas/' ~ time ~ '.png';

        ok $fpath.IO.spurt($captchabuf), 'save to ' ~ $fpath;
    }
    else {
        skip-rest 'captcha method throws exception';
    }
}, 'Check captcha method - successful run';

# Check captcha method - exceptions
subtest {
    plan 3;

    dies-ok {
        $capi.captcha(
            :route('/captcha/dies'),
            :tick(1),
            :sharedobj(Nil),
            :match(Hash.new)
        )
    }, 'dies with null sharedobj';

    dies-ok {
        $capi.captcha(
            :route('/captcha/dies'),
            :tick(1),
            :sharedobj($ctrl.sharedobj),
            :match(Hash.new)
        )
    }, 'dies with null match';

    dies-ok {
        $capi.captcha(
            :route('/captcha/dies'),
            :tick(1),
            :sharedobj($ctrl.sharedobj),
            :match({query => '?foobar123'})
        )
    }, 'dies with wrong query';
}, 'Check captcha method - exceptions';

# Check captcha method - exception on blank payload
subtest {
    plan 1;

    my $cqry     = $ctrl.sharedobj<utilobj>.do_encrypt('0123456789');
    my $mockeduo = mocked(
        Pheix::Utils,
        returning => {
            test => True,
            jsonobj => $ctrl.sharedobj<jsonobj>,
            lzwsz => 97000,
            show_captcha => (Str),
        }
    ).new;

    my $c = Pheix::Controller::Basic.new(
        :apirobj(Nil),
        :mockedfcgi($fcgi),
        :mockedutils($mockeduo),
        :test(True),
    );

    throws-like {
        $capi.captcha(
            :route('/captcha/?' ~ $cqry),
            :tick(1),
            :sharedobj($c.sharedobj),
            :match({query => q{?} ~ $cqry})
        )
    },
    Exception,
    message => /'Captcha generation error'/,
    'captcha method throws exception on blank payload';
}, 'Check captcha method - exception on blank payload';

done-testing;

# November
# ========
#
# November is Copyright (C) 2008-2009, the November contributors (as listed in
# CONTRIBUTING).
#
# LICENSE INFORMATION
# -------------------
#
# November is free software; you can redistribute it and/or modify it under the
# terms of the Artistic License 2.0.  (Note that, unlike the Artistic License
# 1.0, version 2.0 is GPL compatible by itself, hence there is no benefit to
# having an Artistic 2.0 / GPL disjunction.)
#
# CONTRIBUTING
# ------------
#
# We'll hand out commit bits liberally. If you want to contribute, create an
# account on github.org and send your github handle to Johan
# (johan.viklund@gmail.com).  Patches are ok too, if you prefer those.  See
# docs/COMMITTERS if these thoughts intrigue you.
#
# CONTACT
# -------
#
# Google group: november-wiki@googlegroups.com
# IRC channel: #november-wiki over at irc.freenode.org
# Github: https://github.com/viklund/november

use v6.d;
use lib './lib';
use Test;
use Pheix::Addons::November::CGI;

plan 2;

subtest {
    plan 20;
    ok(1,'We use Pheix::Addons::November::CGI and we are still alive');
    my $cgi = Pheix::Addons::November::CGI.new;
    ok(
        $cgi ~~ Pheix::Addons::November::CGI,
        'an instance is of the right class',
    );
    my @queries = (
        'test=',
          { :test('') } ,
        'test=1',
          { :test('1') },
        'test=2&params=2',
          { :test('2'), :params('2') },
        'test=3&params=3&words=first+second',
          { :test('3'), :params('3') :words('first second') },
        'test=4&params=3&words=first+%41+second',
          { :test('4'), :params('3') :words('first A second') },
        'test=5&params=3&words=first%0Asecond',
          { :test('5'), :params('3') :words("first\nsecond") },
        'test=foo&test=bar',
          { test => [<foo bar>] },
        'test=2;params=2',
          { :test('2'), :params('2') },
        'test=3;params=3;words=first+second',
          { :test('3'), :params('3'), :words('first second') },
        'test=4;params=3&words=first+%41+second',
          { :test('4'), :params('3'), :words('first A second') },
        );

    for @queries -> $in, $expected {
        my $c = Pheix::Addons::November::CGI.new;
        $c.parse_params($in);
        is-deeply($c.params, $expected, 'Parse param: ' ~ $in);
    }

    my @keywords = (
        'foo',
          ['foo'],
        'foo+bar+her',
          ['foo','bar','her'],
        );

    for @keywords -> $in, $expected {
        $cgi.parse_params($in);
        is-deeply(
            $cgi.keywords,
            $expected ,
            'Parse param (keywords): ' ~ $in,
        );
    }

    $cgi = Pheix::Addons::November::CGI.new();

    my @add_params = (
        :key1<val> , { :key1<val> },
        :key2<val> , { :key1<val>,      :key2<val> },
        :key1<val2>, { key1 => [<val val2>], :key2<val> },
        :key3<4>   , { key1 => [<val val2>], :key2<val>, :key3<4> },
        :key4<4.1> , {
                key1 => [<val val2>],
                :key2<val>,
                :key3<4>,
                :key4<4.1>,
        },
    );

    for @add_params -> $in, $expected {
        my $key = $in.key;
        my $val = $in.value;
        $cgi.add_param($key, $val);
        is-deeply(
            $cgi.params,
            $expected,
            "Add kv: :{$key}<" ~ ($val or '') ~ ">",
        );
    }

    is(  $cgi.param('key3'), '4', 'Test param' );
}, 'Basic CGI tests';

subtest {
    my @t =
        '%61'                  => 'a',
        '%C3%A5'               => 'å',
        '%C4%AC'               => 'Ĭ',
        '%C7%82'               => 'ǂ',
        '%E2%98%BA'            => '☺',
        '%E2%98%BB'            => '☻',
        'alla+snubbar'         => 'alla snubbar',
        'text%61+abc'          => 'texta abc',
        'unicode+%C7%82%C3%A5' => 'unicode ǂå',
        '%25'                  => '%',
        '%25+25'               => '% 25',
        '%25rr'                => '%rr',
        '%2561'                => '%61',
        ;
    plan +@t;
    for @t {
        my $ans = Pheix::Addons::November::CGI::unescape( ~.key );
        ok( $ans eq .value, 'Decoding ' ~ .key )
            or say "GOT: {$ans.perl}\nEXPECTED: {.value.perl}";

    }
}, 'URL encode test';

#!/bin/sh

echo -n "foo=bar" |
REQUEST_METHOD='POST' \
SERVER_NAME='test.foo' \
TEST_RESULT='{"foo" => "bar"}' \
TEST_NAME='Post foo=bar' \
perl6 ./t/cgi/cgi_post.p6 test_1;

echo -n "foo=bar&boo=her" |
REQUEST_METHOD='POST' \
SERVER_NAME='test.foo' \
TEST_RESULT='{"foo" => "bar", "boo" => "her"}' \
TEST_NAME='Post foo=bar&boo=her' \
perl6 ./t/cgi/cgi_post.p6 test_2;

echo -n "test=foo&test=bar" |
REQUEST_METHOD='POST' \
SERVER_NAME='test.foo' \
TEST_RESULT='{"test" => ["foo", "bar"] }' \
TEST_NAME='Post test=foo&test=bar' \
perl6 ./t/cgi/cgi_post.p6 test_3;

echo -n "test=foo&test=bar&foo=bar" |
REQUEST_METHOD='POST' \
SERVER_NAME='test.foo' \
TEST_RESULT='{"test" => ["foo", "bar"], "foo" => "bar"}' \
TEST_NAME='Post test=foo&test=bar&foo=bar' \
perl6 ./t/cgi/cgi_post.p6 test_4;

echo -n "test=foo" |
REQUEST_METHOD='POST' \
SERVER_NAME='test.foo' \
QUERY_STRING="boom=bar" \
TEST_RESULT='{"test" => "foo", "boom" => "bar" }' \
TEST_NAME='Post test=foo Get boom=bar (test get and post mix)' \
perl6 ./t/cgi/cgi_post.p6 test_5;

echo -n "test=foo" |
REQUEST_METHOD='POST' \
SERVER_NAME='test.foo' \
QUERY_STRING="test=bar" \
TEST_RESULT='{"test" => ["bar", "foo"] }' \
TEST_NAME='Post test=foo Get test=bar (test get and post mix)' \
perl6 ./t/cgi/cgi_post.p6 test_6;

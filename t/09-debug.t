use v6.d;
use Test;
use lib 'lib';

plan 1;

use Pheix::View::Debug;
my Any $obj = Nil;

# Check debug
subtest {
    plan 3;
    $obj = Pheix::View::Debug.new;
    my Str $path = '';
    my Str $env  = $obj.show_env_vars;
    ok( $env, 'show_env_variables() returns non-empty value' );
    ok( ( $env ~~ m/ \%\*ENV\{PATH\} / ), '%*ENV{PATH} is found' );

    if ($env ~~ m/ '%*ENV{PATH} = ' ( . ** 1..* ) '<br>' /) {
        $path = $0.Str;
    }

    ok( $path, 'PATH is not empty' );
}, 'Check debug';

done-testing;

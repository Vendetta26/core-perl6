<h1>Лыжный поход по Кимрскому району Тверской области. 3-6 января 2011 г.</h1>

<p>&nbsp;</p>

<hr />
<hr />
<p><br />
&nbsp;&nbsp;&nbsp;&nbsp; Изначально планировался поход от города Кимры до Твери, но обстоятельства сложились так, что мы дошли только до урочища Архангельское. В итоге поход занял 4 полных ходовых дня с 3-мя ночевками. Состав группы: Павел, Степан и Антон Ляховы.<br />
&nbsp;&nbsp;&nbsp;&nbsp;Так как до этого путешествия у меня не было опыта прохождения многодневных лыжных походов, понадобилось изучить техническую сторону этого предприятия. Позвонил товарищу, из разговора выяснилось, что для похода необходимы бахилы, специальные туристические лыжи. Обычные беговые лыжи, как выяснилось позже, вообще бы поставили под угрозу всю экспедицию, так как дополнительная нагрузка, вызванная весом рюкзака легко бы сломала их на глубоком снегу, так как тонкие беговые лыжи не обладали достаточной площадью для распределения нагрузки на снег).&nbsp;<br />
&nbsp;&nbsp;&nbsp;&nbsp; Современные туристические лыжи оказались не такими тяжелыми и тихоходными как о них говорили. Это были средней ширины (7 см) деревянные лыжи с пластиковой скользящей частью с насечками в средней части. Они похожи на обычные полупластиковые беговые лыжи, только чуть шире (Лыжи НЛК &laquo;Турист&raquo;, стоили 1600 р в &laquo;Турине&raquo;). Сначала было желание сэкономить и купить подержанные деревянные за 500 р, но сходив в магазин, я решил взять новые &ndash; не охото было корячиться на старых, 20-летних лыжах. Также были приобретены крепления &laquo;Азимут&raquo; (700 с чем-то руб.), бахилы &laquo;Снаряжение&raquo; (около 950 р), металлические лыжные палки с широкими кольцами + запасную пару колец (вместе за 600 р). На этом покупки закончились. Остальное брали из имеющегося дома снаряжения. Также с собой был взят кусок полиэтилена, который подкладывался под палатку, тем самым превращая снег в отличный утеплитель. Для теплоизоляции брали по 2 пенки на человека &ndash; они прекрасно защищали от теплопотерь. Я взял с собой большой американский зимний спальник &ndash; он занимал пол рюкзака, но в нем было комфортно и тепло. Антон со Степаном взяли по 2 летних спальника и тоже не жаловались на холод.&nbsp;<br />
&nbsp;&nbsp;&nbsp;&nbsp; В этом походе мы провели несколько экспериментов по пищевой части &ndash; во первых, отказались от тяжелых банок с тушенкой, взяв с собой куски копченого шпика с большой прослойкой мяса. Оказалось намного легче и вкуснее (единственно, для лета это наверное не прокатит &ndash; в жаре мясо может испортиться). Из еды взяли гречку, рис, макароны, горох &ndash; всего понемногу. Стандартные нормы по 100 гр. крупы на человека нам не особо подошли, опытным путем было доказано что нам чтобы нормально на ужин хватало полкило гречки на троих + грамм по 60-70 копченого сала. Еды взяли на всякий случай побольше. В качестве резерва был взят рис и горох, данные крупы в смеси образуют неплохую еду, богатую энергией (поев вечером этой чудо каши мы получили столько энергии, что утром не хотелосьесть). Тем, кому лень тащить лишнюю еду рекомендую на утро овсянку &laquo;Экстра&raquo; и халву, а на вечер рисово-гороховую кашу. На таком топливе можно целый день по бурелому шататься с рюкзаком и лыжами.<br />
&nbsp;&nbsp;&nbsp;&nbsp; Несколько слов о технике передвижения на лыжах с рюкзаком. Мы использовали тактику, взятую из интеринет-обзора: периодически менять впереди идущего (самый затратный по силам участок). В итоге получается так: впереди идти тяжелее всего, человек шедший за ним уже шел легко, третий же человек скользил по лыжне без особых проблем. Причем получается так что скорость передвижения при пробивке лыжни примерно в три раза меньше чем скорость езды по готовой лыжне.</p>

<hr />
<table>
	<tbody>
		<tr>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/karta.jpg"><img alt="" src="http://drojj-taigi.narod.ru/kimry/tkarta.jpg" style="height:216px; width:510px" /></a></td>
		</tr>
	</tbody>
</table>

<hr />
<p>.</p>

<p>Итак, техническое отступление окончено, приступаю к описанию событий.<br />
<br />
Как ранее было сказано, маршрут планировался от Кимры до Твери. Так как местность по всему протяжению пути была достаточно пологая, у меня появилась мысль посмотреть прогноз погоды касательно направления ветров. Выяснилось, что на недельном промежутке времени ветра должны были дуть преимущественно с востока на запад или на северо-запад. Очевидно, что лучше было идти от Кимров. Так и сделали.&nbsp;<br />
&nbsp;&nbsp;&nbsp;&nbsp; Рано утром мы загрузились в электричку до Савелова. Билеты взяли не зря &ndash; даже в такую рань прошли контроллеры и мы гордо показали им свои билеты :-)</p>

<table>
	<tbody>
		<tr>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030451.jpg" target="blank"><img src="http://drojj-taigi.narod.ru/kimry/tkimri1030451.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030452.jpg" target="blank"><img src="http://drojj-taigi.narod.ru/kimry/tkimri1030452.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030453.jpg" target="blank"><img src="http://drojj-taigi.narod.ru/kimry/tkimri1030453.jpg" /></a></td>
		</tr>
	</tbody>
</table>

<p>&nbsp;&nbsp;&nbsp;&nbsp; На станцию Савелово прибыли около 9 утра. Вышли из электрички, осмотрелись &ndash; пейзажи вокруг станции напоминают виды дальних станций за Нижним Тагилом. Кругом разруха. Но мы бодрые и довольные жизнью встретили местного человека и расспросили как бы нам перебраться через Волгу и попасть в старую часть города Кимры. Оказалось, что от станции ходят автобусы, и мы решили не тащиться пешком 5 км по унылым дорогам, а быстро доехать до центра на автобусе. На тот момент билеты стоили около 16 рублей.&nbsp;<br />
&nbsp;&nbsp;&nbsp;&nbsp; Минут за 10 доехали до улицы Кирова, высадились, оставили рюкзаки и Степу с ними и пошли с Антоном в рыболовно-охотничий магазин, который очень кстати был нами замечен. Наличие сего магазина нас весьма порадовало, тем более он еще и работал в такую рань. Антон купил себе там носки, которые забыл дома, также купили пару амоновских шапок и кружку (я второпях забыл взять ее дома, понадеясь, что вырежу из подручных средств).</p>

<table>
	<tbody>
		<tr>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030454.jpg" target="blank"><img src="http://drojj-taigi.narod.ru/kimry/tkimri1030454.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030455.jpg" target="blank"><img src="http://drojj-taigi.narod.ru/kimry/tkimri1030455.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030456.jpg" target="blank"><img src="http://drojj-taigi.narod.ru/kimry/tkimri1030456.jpg" /></a></td>
		</tr>
	</tbody>
</table>

<p><br />
<br />
&nbsp;&nbsp;&nbsp;&nbsp; Немного о самом городе. Первое упоминание о Кимрах было в 1546 году в грамоте Ивана Грозного. А по данных археологических исследований данное поселение существовало еще с эпохи мезолита. На протяжении всей своей истории поселение славилось своими мастерами. В 19-начале 20 века село славилось своими кожевенными производствами. Кимрские сапоги считались лучшими в России. Наличие реки Волги давало селу преимущества в торговом деле. В общем городок не был бедным.&nbsp;<br />
&nbsp;&nbsp;&nbsp;&nbsp; Статус города село Кимры приобрело Постановлением Временного правительства от 3(16) июня в 1917 году. В конце 19 &ndash; начале 20 века в Кимрах было развито строительство, благодаря которому мы и наблюдали прекрасные творения зодчества с стиле &laquo;модерн&raquo;.&nbsp;<br />
&nbsp;&nbsp;&nbsp;&nbsp; Сейчас город переживает не лучшие годы. Подробнее о Кимрах можно прочитать на официальном портале администрации города&nbsp;<a href="http://www.adm-kimry.ru/" target="_blank">http://www.adm-kimry.ru/</a>&nbsp;или на сайте&nbsp;<a href="http://kimry2009.ru/index/0-14" target="_blank">http://kimry2009.ru/index/0-14</a>.</p>

<table>
	<tbody>
		<tr>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030472.jpg" target="blank"><img src="http://drojj-taigi.narod.ru/kimry/tkimri1030472.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030473.jpg" target="blank"><img src="http://drojj-taigi.narod.ru/kimry/tkimri1030473.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030474.jpg" target="blank"><img src="http://drojj-taigi.narod.ru/kimry/tkimri1030474.jpg" /></a></td>
		</tr>
	</tbody>
</table>

<p>&nbsp;&nbsp;&nbsp;&nbsp; Проходя по главной улице мы любовались деревянные строения в стиле &laquo;модерн&raquo; с башенками и причудливыми наличниками &ndash; подобные строения я видел лишь в иллюстрациях к старым русским сказкам. Обидно, что многие дома, представляющие безусловную ценность как памятники архитектуры, стоят в полуразрушенном состоянии. Вскоре улица постепенно вывела нас к окраинам. Минуя завод по производству теплообменного и радиаторного оборудования, мы свернули влево в сторону леса.</p>

<table>
	<tbody>
		<tr>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030475.jpg" target="blank"><img src="http://drojj-taigi.narod.ru/kimry/tkimri1030475.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030476.jpg" target="blank"><img src="http://drojj-taigi.narod.ru/kimry/tkimri1030476.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030477.jpg" target="blank"><img src="http://drojj-taigi.narod.ru/kimry/tkimri1030477.jpg" /></a></td>
		</tr>
	</tbody>
</table>

<table>
	<tbody>
		<tr>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030480.jpg" target="blank"><img alt="Пивасик" src="http://drojj-taigi.narod.ru/kimry/tkimri1030480.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030481.jpg" target="blank"><img src="http://drojj-taigi.narod.ru/kimry/tkimri1030481.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030482.jpg" target="blank"><img src="http://drojj-taigi.narod.ru/kimry/tkimri1030482.jpg" /></a></td>
		</tr>
	</tbody>
</table>

<p>&nbsp;&nbsp;&nbsp;&nbsp; Наконец надели лыжи &ndash; до того не было времени их примерить и были опасения по поводу того правильно ли мы подготовили лыжи, но к счастью все поучилось без изъяна &ndash; лыжи не спадали с бахил и ехали превосходно. Палки тоже не подкачали, единственно что, Степа умудрился разломать пряжку на ремешке от палки, но поломку он устранил посредствам связывания двух концов ремешка.</p>

<table>
	<tbody>
		<tr>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030483.jpg" target="blank"><img src="http://drojj-taigi.narod.ru/kimry/tkimri1030483.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030484.jpg" target="blank"><img src="http://drojj-taigi.narod.ru/kimry/tkimri1030484.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030485.jpg" target="blank"><img src="http://drojj-taigi.narod.ru/kimry/tkimri1030485.jpg" /></a></td>
		</tr>
	</tbody>
</table>

<p>&nbsp;&nbsp;&nbsp;&nbsp; По грунтовой дороге, достаточно хорошо наезженной, но без соли, дошли до деревни Шутово. От Шутово встали на снегоходный след и достаточно легко вышли по нему к кладбищу, что между деревней Воробьево и селом Филлиппово. От Филиппово снегоходная колея шла четко по намеченному мною маршруту. Колея была большой удачей, благодаря ей, мы без проблем проехали через лес. Снегоходчики обычно пропиливают дорогу среди буреломов и а гусеницы снегоходов уминают снег, все это значительно облегчает передвижение на лыжах.<br />
&nbsp;&nbsp;&nbsp;&nbsp; Так как мы много времени затратили на осмотр города, в первый день мы не особо много прошли. Первая ночевка была в лесу восточнее Горычкино. Палатка, поставленная на снежную подушку, накрытую полиэтиленом оказалась теплой и уютной. По утру осмотрев снежную площадку под палаткой я обнаружил что сильного обледенения снега не было, значит прослойка из двух ковриков оказалась вполне эффективной.</p>

<table>
	<tbody>
		<tr>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030490.jpg" target="blank"><img src="http://drojj-taigi.narod.ru/kimry/tkimri1030490.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030491.jpg" target="blank"><img src="http://drojj-taigi.narod.ru/kimry/tkimri1030491.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030492.jpg" target="blank"><img alt="буреломный участок пути" src="http://drojj-taigi.narod.ru/kimry/tkimri1030492.jpg" /></a></td>
		</tr>
	</tbody>
</table>

<p>&nbsp;&nbsp;&nbsp;&nbsp; Утром 4 января встали по будильнику в 7-30, быстро развели костер, сварили овсянку (на утро рекомендую для походов &ndash; мало весит, быстро готовится). Вышли на маршрут около 10 утра.&nbsp;<br />
&nbsp;&nbsp;&nbsp;&nbsp; У деревни Горычкино снегоходная колея свернула с нашего маршрута и нам пришлось пробивать дорогу самим &ndash; скорость естественно уменьшилась. Основная проблема была в том, что было жарко идти. Несмотря на легкую одежду, потооделение было достаточно обильным, и, чтобы не промокнуть окончательно, раз в 700-800 метров мы делали остановки для охлаждения организма. Тут я и подумал о целесообразности покупки более легкого снаряжении для зимник походов. Летом, как ни странно, эта проблема не сильно беспокоит, а зимой вспотев можно и заболеть. От Горычкино дошли до Кореньково, переправившись через реку Кимрку по мощному бревенчатому мосту.<br />
&nbsp;&nbsp;&nbsp;&nbsp; По карте от Кореньково до Дымово около двух километров. Мы планировали пройти по лесной дороге. Часть этой заброшенной дороги оказалась вполне пригодной для прохода. Но после просеки, где по карте была ЛЭП (сейчас её уже нет) дорога превратилась в бурелом, причем такой, что мы почти весь остаток дня потратили на его преодоление. В итоге кое как выбрались к реке и пошли вдоль неё. Надо было изначально после моста свернуть и пойти по реке, тогда дошли бы до руин церкви в этот же день. На будущее учту этот опыт. А так, в принципе испытать на себе что такое по сугробам пробираться с рюкзаком и лыжами было полезно. На ночевку остановились в лесу у реки. Место для ночевки выбрали неплохое: вокруг было много сухостоя из ольхи, которые легко ломались, так что топором пользоваться не пришлось.<br />
<br />
&nbsp;&nbsp;&nbsp;&nbsp; 5 января встали около 8 утра. Внутренняя палатка была мокрой от конденсата, на внешней с внутренней стороны образовался слой изморози на котором мы нацарапали надпись &laquo;доброе утро&raquo;. На этот раз сборы прошли намного быстрее: Степа с Антоном занимались костром и приготовлением завтрака, а я складывал палатку.</p>

<table>
	<tbody>
		<tr>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030496.jpg" target="blank"><img alt="Дымово" src="http://drojj-taigi.narod.ru/kimry/tkimri1030496.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030497.jpg" target="blank"><img src="http://drojj-taigi.narod.ru/kimry/tkimri1030497.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030498.jpg" target="blank"><img src="http://drojj-taigi.narod.ru/kimry/tkimri1030498.jpg" /></a></td>
		</tr>
	</tbody>
</table>

<table>
	<tbody>
		<tr>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030560.jpg"><img alt="панорама вид около Дымово" src="http://drojj-taigi.narod.ru/kimry/tkimri1030560.jpg" style="height:177px; width:510px" /></a></td>
		</tr>
	</tbody>
</table>

<table>
	<tbody>
		<tr>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030561.jpg"><img alt="Вид на Дымово" src="http://drojj-taigi.narod.ru/kimry/tkimri1030561.jpg" style="height:141px; width:510px" /></a></td>
		</tr>
	</tbody>
</table>

<p><br />
&nbsp;&nbsp;&nbsp;&nbsp; Вышли около10 часов утра. Выходя из лагеря часть пути по лесу прошли пешком, потом выйдя на более менее свободное от деревьев место надели лыжи и направились вдоль реки в надежде выйти на Дымово. На снегу слегка читалась дорога, мы вышли на неё, уже прошли достаточное расстояние а деревни все нет. Дорога все больше походила на деревенскую улицу, но домов не было &ndash; только небольшие холмики, поросшие сухой травой, пробивающейся сквозь сугробы. Проверили по джипиэсу &ndash; мы в центре деревни.</p>

<table>
	<tbody>
		<tr>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030500.jpg" target="blank"><img alt="Дымово" src="http://drojj-taigi.narod.ru/kimry/tkimri1030500.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030501.jpg" target="blank"><img alt="Лыжня" src="http://drojj-taigi.narod.ru/kimry/tkimri1030501.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030502.jpg" target="blank"><img alt="Бетонный мост через Кимрку" src="http://drojj-taigi.narod.ru/kimry/tkimri1030502.jpg" /></a></td>
		</tr>
	</tbody>
</table>

<p>&nbsp;&nbsp;&nbsp;&nbsp; Наша цель на сегодня &ndash; достижение руин старинной церкви Михаила Архангела (построена между 1826 и 1839 г.) в селе Архангельское (сейчас это урочище). Посовещавшись мы решили дойти до руин и повернуть обратно в Кимры по проторенной нами лыжне.&nbsp;<br />
&nbsp;&nbsp;&nbsp;&nbsp; До Твери решили не идти по двум причинам: во первых, проходя болотистые участки я подметил, что болота не замерзли должным образом, и местами палки проваливались в жижу, скрытую под рыхлым снегом, во вторых, мы потеряли день на проходку буреломного участка так что появились опасения о том что путь до Твери может серьезно затянуться а Антону и Степе надо было ехать домой в районе 7 января.</p>

<table>
	<tbody>
		<tr>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030503.jpg" target="blank"><img src="http://drojj-taigi.narod.ru/kimry/tkimri1030503.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030505.jpg" target="blank"><img src="http://drojj-taigi.narod.ru/kimry/tkimri1030505.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030506.jpg" target="blank"><img alt="Дорога от Дымово до Золотилово" src="http://drojj-taigi.narod.ru/kimry/tkimri1030506.jpg" /></a></td>
		</tr>
	</tbody>
</table>

<table>
	<tbody>
		<tr>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030509.jpg" target="blank"><img alt="Привал на дороге от Дымово до Золотилово" src="http://drojj-taigi.narod.ru/kimry/tkimri1030509.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030510.jpg" target="blank"><img alt="Золотилово" src="http://drojj-taigi.narod.ru/kimry/tkimri1030510.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030514.jpg" target="blank"><img alt="двор а Золотилове" src="http://drojj-taigi.narod.ru/kimry/tkimri1030514.jpg" /></a></td>
		</tr>
	</tbody>
</table>

<p>От Дымово до урочища есть прямая дорога, но наученные опытом предыдущего перехода через заваленную деревьями лесную дорогу, мы решили идти обходным путем через Золотилово.&nbsp;<br />
&nbsp;&nbsp;&nbsp;&nbsp; От Дымово до Золотилова идет хорошая насыпная дорога, с недавними следами от трактора. Помимо тракторных следов было еще две цепочки следов человека, причем туда и обратно. Антон сделал предположение, что насыпь вполне могла быть сделана для прокладки железнодорожного полотна. Поля вдоль дороги поросли молодыми деревьями &ndash; природа постепенно возвращает себе свои территории, ранее отвоеванные человеком у леса. Дошли до Золотилова &ndash; результат тот-же &ndash; от домов почти ничего не осталось. Только в конце деревни был небольшой двор, не обнесенный забором, который, судя по всему, летом служил дачей. Кстати, следы, к которым мы присоединились на дороге шли конца деревенской улицы,разварачивались, не заходя в тот единственный двор, вели шли обратно.</p>

<table>
	<tbody>
		<tr>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030511.jpg" target="blank"><img alt="Золотилово" src="http://drojj-taigi.narod.ru/kimry/tkimri1030511.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030512.jpg" target="blank"><img alt="Золотилово" src="http://drojj-taigi.narod.ru/kimry/tkimri1030512.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030513.jpg" target="blank"><img alt="Золотилово" src="http://drojj-taigi.narod.ru/kimry/tkimri1030513.jpg" /></a></td>
		</tr>
	</tbody>
</table>

<p>&nbsp;&nbsp;&nbsp;&nbsp; Время шло к вечеру, мы все ближе продвигались к своей цели пробивая нетронутую снежную целину и к половине четвертого мы были на подступах к Архангельскому. Урочище имело одухотворенный вид, отличный от простых лесных полян, имело особую энергетику. К селу со всех сторон в прошлом подходило много дорог&ndash; понятное дело, церковь одна на несколько деревень, но сейчас все они заросли и почти не проезжи.</p>

<table>
	<tbody>
		<tr>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030515.jpg" target="blank"><img alt="Закат" src="http://drojj-taigi.narod.ru/kimry/tkimri1030515.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030516.jpg" target="blank"><img alt="Лосиные и заячьи следы" src="http://drojj-taigi.narod.ru/kimry/tkimri1030516.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030518.jpg" target="blank"><img alt="заячья тропа до Подходим к руинам церкви" src="http://drojj-taigi.narod.ru/kimry/tkimri1030518.jpg" /></a></td>
		</tr>
	</tbody>
</table>

<table>
	<tbody>
		<tr>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030517.jpg" target="blank"><img alt="Закат" src="http://drojj-taigi.narod.ru/kimry/tkimri1030517.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030522.jpg" target="blank"><img alt="Урочище Архангельское" src="http://drojj-taigi.narod.ru/kimry/tkimri1030522.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri10305123.jpg" target="blank"><img alt="заячья тропа до Подходим к руинам церкви" src="http://drojj-taigi.narod.ru/kimry/tkimri1030523.jpg" /></a></td>
		</tr>
	</tbody>
</table>

<p>&nbsp;&nbsp;&nbsp;&nbsp; Вскоре, сквозь лес мы увидели руины церкви. Судя по характеру повреждений, мы сделали вывод, что церковь была подорвана про заложении взрывчатки в подвале, по углам основного строения и под нефами, апсидами и закомарами. В итоге уцелела только центральная часть. Судя по надписям на штукатурке, оставленным прежними посетителями этих мест храм пустовал еще с 1960-х годов.</p>

<table>
	<tbody>
		<tr>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030524.jpg" target="blank"><img alt="дорога от ДоАрхангельское" src="http://drojj-taigi.narod.ru/kimry/tkimri1030524.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030525.jpg" target="blank"><img alt="Руины" src="http://drojj-taigi.narod.ru/kimry/tkimri1030525.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri10305126.jpg" target="blank"><img src="http://drojj-taigi.narod.ru/kimry/tkimri1030526.jpg" /></a></td>
		</tr>
	</tbody>
</table>

<p>&nbsp;&nbsp;&nbsp;&nbsp; Если представить все это сооружение в былом величии, это был большой, красивый храм.</p>

<table>
	<tbody>
		<tr>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030527.jpg" target="blank"><img alt="Внутри храма" src="http://drojj-taigi.narod.ru/kimry/tkimri1030527.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030528.jpg" target="blank"><img alt="Остатки фресок" src="http://drojj-taigi.narod.ru/kimry/tkimri1030528.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030531.jpg" target="blank"><img alt="" src="http://drojj-taigi.narod.ru/kimry/tkimri1030531.jpg" /></a></td>
		</tr>
	</tbody>
</table>

<table>
	<tbody>
		<tr>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030529.jpg" target="blank"><img alt="Подвес для люстры" src="http://drojj-taigi.narod.ru/kimry/tkimri1030529.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030530.jpg" target="blank"><img src="http://drojj-taigi.narod.ru/kimry/tkimri1030530.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030532.jpg" target="blank"><img src="http://drojj-taigi.narod.ru/kimry/tkimri1030532.jpg" /></a></td>
		</tr>
	</tbody>
</table>

<table>
	<tbody>
		<tr>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030533.jpg" target="blank"><img src="http://drojj-taigi.narod.ru/kimry/tkimri1030533.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030534.jpg" target="blank"><img alt="Крепежи от решеток" src="http://drojj-taigi.narod.ru/kimry/tkimri1030534.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030535.jpg" target="blank"><img alt="заячья тропа до Покрашенная стена с нацорапанными надписями" src="http://drojj-taigi.narod.ru/kimry/tkimri1030535.jpg" /></a></td>
		</tr>
	</tbody>
</table>

<p><br />
&nbsp;&nbsp;&nbsp;&nbsp; С одной стороны смотришь и думаешь о том что жалко такие заброшенные села, а с другой &ndash; вся эта красота имеет особую ноту загадки, печали и одиночества которой не ни в одном жилом селении. Поэтому мне нравятся такие места &ndash; приятно соприкоснуться со стариной, помыслить о вечном.&nbsp;<br />
&nbsp;&nbsp;&nbsp;&nbsp; Мы обошли все развалины, перекусили и примерно в 16-30 направились в обратный путь по проторенной нами лыжне. По лыжне, естественно ехать было намного быстрее и легче &ndash; на обратный путь мы затратили около 2 часов, в то время как добирались до места 6 часов. Часть пути шли еже по темноте с налобными фонарями &ndash; это был особый аттракцион: вспудренная снежная поверхность и идущий мелкий снежок искрились подобно звездам и чувствовал себя мифическим звездным медведем, пробирающимся сквозь созвездия в космический мрак. Остановиться на ночевку решили в том же месте, откуда сегодня стартовали: место было удобное и обжитое, дрова тоже имелись. Так как пришли на стоянку поздно сегодня легли около 12.<br />
&nbsp;&nbsp;&nbsp;&nbsp; Утром 6 января стали как обычно около 7-30. Спальники немного отсырели, но холодно не было. Получается, если идти с теми же спальниками на много дней, их придется просушивать на костре раз в 3 дня. Так как часть пути до места стоянки уже проходила по пойме реки, мы пошли по этой лыжне до того момента как она не привела нас к месту, где мы вышли из бурелома. Далее мы пошли вдоль реки до моста &ndash; старались идти по руслу, так как там более тонкий слой снега. После того как на одном из участков лыжная палка провалилась легко продавив лед и образовав небольшую полынью, мы решили прижаться поближе к берегу.</p>

<table>
	<tbody>
		<tr>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030539.jpg" target="blank"><img alt="Тонкий лед Кимрки" src="http://drojj-taigi.narod.ru/kimry/tkimri1030539.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030540.jpg" target="blank"><img alt="Заболоченный берег Кимрки" src="http://drojj-taigi.narod.ru/kimry/tkimri1030540.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030541.jpg" target="blank"><img alt="заячья тропа до Мост через Кимрку около Горычкино" src="http://drojj-taigi.narod.ru/kimry/tkimri1030541.jpg" /></a></td>
		</tr>
	</tbody>
</table>

<table>
	<tbody>
		<tr>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030542.jpg" target="blank"><img alt="Мост через Кимрку около Горычкино" src="http://drojj-taigi.narod.ru/kimry/tkimri1030542.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030543.jpg" target="blank"><img alt="у доски почета" src="http://drojj-taigi.narod.ru/kimry/tkimri1030543.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030544.jpg" target="blank"><img alt="заячья тропа до станции Мытищи" src="http://drojj-taigi.narod.ru/kimry/tkimri1030544.jpg" /></a></td>
		</tr>
	</tbody>
</table>

<p>До моста дошли довольно быстро, а от моста по укатанной дороге дошли до снегоходной колеи, а там рукой подать до Филлиппово. Прошли через Филлиппово, останавливаясь для фотографирования деревянных домов и старинной кирпичной Сергеевской церкви, отреставрированной в 2004 году. Тут мы встретили первого человека за 4 дня похода, и тот был похож на зомби. Странный человек с коричневым, обмороженным лицом в телогрейке и ватных штанах вез за собой санки с поклажей. Я поздоровался с ним &ndash; он толи не услышал, толи проигнорировал, потом я еще раз поздоровался, на этот раз услышал ответное хриплое приветствие. От Филиппово до дороги в Кимры идет дорога, пересекающая Кимрку по мосту (моста на старой карте не было &ndash; был брод).</p>

<table>
	<tbody>
		<tr>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030554.jpg" target="blank"><img alt="дорога от Диллиппово" src="http://drojj-taigi.narod.ru/kimry/tkimri1030554.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030548.jpg" target="blank"><img alt="Филлиппово" src="http://drojj-taigi.narod.ru/kimry/tkimri1030548.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030550.jpg" target="blank"><img alt="Сергиевская церковь в селе Филиппово Тверской области" src="http://drojj-taigi.narod.ru/kimry/tkimri1030550.jpg" /></a></td>
		</tr>
	</tbody>
</table>

<table>
	<tbody>
		<tr>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030551.jpg" target="blank"><img alt="дорога от Дом в Филлиппово" src="http://drojj-taigi.narod.ru/kimry/tkimri1030551.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030553.jpg" target="blank"><img alt="Дом без трубы в Филлиппово" src="http://drojj-taigi.narod.ru/kimry/tkimri1030553.jpg" /></a></td>
			<td style="background-color:#d0cfcd"><a href="http://drojj-taigi.narod.ru/kimry/kimri1030552.jpg" target="blank"><img src="http://drojj-taigi.narod.ru/kimry/tkimri1030552.jpg" /></a></td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<p>Далее прошли по дороге по городу и дошли до улицы, откуда стартовали. В 6 вечера мы уже сидели в электричке. В итоге с учетом прогулок по городу всего прошли 45 км.</p>

<p><br />
В целом могу сказать, что места мне очень понравились, и есть смысл побывать там не только зимой на лыжах, но и летом пешком или на велосипеде.</p>

<p>&nbsp;</p>

<p><br />
Удачных путешествий!</p>

<p>&nbsp;</p>

<p>&nbsp;&nbsp;<var>22.01.2011. Павел Ляхов</var></p>

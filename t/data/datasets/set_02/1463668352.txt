<div class="head-delimiter"></div>

<div class="content indent">
    <!--content-->
    <div class="thumb-box9" style="margin-top:-30px;">
        <div class="container">
            <p class="title"><span>Объекты</span></p>
	    <p class="object-item-header">Научно-исследовательский испытательный центр подготовки космонавтов имени Ю.А.Гагарина. ЦПК (объект №2)</p>
	    <p class="object-item-descr">В соответствии с распоряжением Правительства Российской Федерации от 1 октября 2008 года № 1435-р в ведении Федерального космического агентства создано федеральное государственное бюджетное учреждение «Научно-исследовательский испытательный центр подготовки космонавтов имени Ю.А. Гагарина» и с 1 июля 2009 года ликвидирован РГНИИЦПК. Начальником Центра был назначен Герой Советского Союза, Герой Российской Федерации, лётчик-космонавт СССР Крикалёв С.К.<br>
        В связи с решением международных партнеров об увеличении с 2009 года членов экипажа МКС с 3-х до 6-и человек существенно возрос объем работ по обеспечению и проведению подготовки российских и иностранных космонавтов (астронавтов). Во 2-й половине 2009 года в Центре проходили подготовку к выполнению космических полетов на транспортном пилотируемом корабле (ТПК) «Союз ТМА» и по программе основных экспедиций МКС 16 экипажей, в состав которых входили 17 российских космонавтов, 12 астронавтов НАСА (США), 2 астронавта Европейского космического агентства (ЕКА) и 4 астронавта ДжАКСА (Япония).</p>
        </div>
    </div>
<div class="thumb-box2">
        <div class="container">
            <p class="title wow fadeInUp"><span style="font-size:16pt;">Фотографии</span></p>
            <div class="row wow fadeInUp">
                <div class="col-lg-8 col-md-10 col-lg-offset-2 col-md-offset-1">
                    <div class="list_carousel1 responsive clearfix ">
                        <ul id="foo1">
                            <li>
                                <figure><img src="images/castiglion/objects/1/pic-3.jpg" alt=""></figure>
                            </li>
                            <li>
                                <figure><img src="images/castiglion/objects/1/pic-4.jpg" alt=""></figure>
                            </li>
                        </ul>
                    </div>
                    <div class="foo-btn clearfix">
                        <div class="pagination" id="foo2_pag"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="content_map">
      <div class="google-map-api"> 
        <div id="map-canvas" class="gmap"></div> 
      </div> 
</section>
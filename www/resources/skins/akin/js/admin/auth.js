import {loadAPI_v2} from '/resources/skins/akin/js/api.js';

function resetContent() {
    var spinner = '<i class="fa fa-cog fa-spin fa-2x fa-fw"></i><span class="sr-only">Loading...</span>';

    riot.unregister('pheix-content')

    jQuery('pheix-content').empty();
    jQuery('.pheix-spinner-page').empty();
    jQuery('.pheix-spinner-page').append(spinner);

    console.log('try to reset content to default state');
}

export function login() {
    var login = jQuery('#ethereumAddress').val() || '';
    var passw = jQuery('#userPassword').val() || '';

    resetContent();

    var component = loadAPI_v2('page', {login: login, password: passw}, 'GET', '/api/admin/auth', '200', '');

    console.log('try to login for admin panel');
}

export function validate() {
    resetContent();

    var component = loadAPI_v2('page', {token: '0x0'}, 'GET', '/api/admin/sess', '200', '');

    console.log('try to validate token');
}

#!/bin/bash

RED=
GREEN=
YELLOW=
NC=
REDBOLD=
DATE=`date '+%Y-%m-%d_%H-%M-%S'`
COVERAGE=0
ITERATOR=0
STARTSTAGE=-1
ENDSTAGE=28
GITVER='undef';
PRECOMP=./lib/.precomp
TESTLOG=./testreport.${DATE}.log
WWW=./www
HASH=`date | md5sum | cut -f1 -d' '`
SUCCESSTOKEN=`md5sum $0 | awk '{ print $1 }'`
SKIPPED=
SKIPPEDSTAGES=()

while getopts "cg:v:s:" opt
do
	case $opt in
	g)
		# echo "at option $opt"
		if [ -d "${OPTARG}" ]
		then
			CURRDIR=`pwd`;
			cd ${OPTARG}
			GITVER=`git log -1 | egrep -i -m 1 -o '([0-9]+\.[0-9]+\.[0-9]+)'`;
			cd ${CURRDIR}
			echo -e "Git root directory: ${GREEN}${OPTARG}${NC}, last commit version: ${GREEN}${GITVER}${NC}"
		fi
	;;
	v)
	    if [ ! -z "${OPTARG}" ]
		then
			CURRVER=${OPTARG}
			echo -e "Current commit version: ${GREEN}${CURRVER}${NC}"
		fi
	;;
	c)
		RED='\e[0;31m'
		GREEN='\e[0;32m'
		YELLOW='\e[0;33m'
		NC='\e[0m'
		REDBOLD='\e[1;31m'
		echo -e "Colors in output are ${GREEN}switch on${NC}!"
	;;
	s)
	    IFSBACK="$IFS"
		IFS=', ' read -r -a SKIPPEDSTAGES <<< "${OPTARG}"
		IFS=$IFSBACK
		echo -e "Got ${#SKIPPEDSTAGES[@]} stages to forced skip: ${OPTARG}"
	;;
	*)
		# echo "No reasonable options found!"
	;;
	esac
done

if [ $? -ne 0 ]
then
	echo -e "[ ${REDBOLD}PANIC:${NC}${RED} command line args processing failure${NC} ]"
	exit 2
fi

check_output () {
	local SUCCESSFUL=
	local OUTPUT="$1"
	local STAGE="$2"
    SUCCESSFUL=`echo $OUTPUT | grep "$SUCCESSTOKEN"`
	if [ ! -z "$SUCCESSFUL" ]; then
     	SKIPPED=`echo $OUTPUT | grep 'SKIP'`
		if [ $STAGE -gt 0 ]; then
		    echo -e "----------- STAGE no.${STAGE} -----------\n${OUTPUT}" | sed -e "s/${SUCCESSTOKEN}//g" >> $TESTLOG
		fi
		return 0
	else
		echo -e "------- ERROR AT STAGE no.${STAGE} -------\n${OUTPUT}" >> $TESTLOG
		echo -e "[ ${REDBOLD}PANIC:${NC}${RED} success token missed${NC} ]"
		return 1
	fi
}

stage_is_skipped () {
    local STAGE="$1"
    local ISSKIPPED=0

    for SKIPSTAGE in "${SKIPPEDSTAGES[@]}"
    do
        if [ "$SKIPSTAGE" = "$STAGE" ]; then
			ISSKIPPED=1
			SKIPPED="# SKIP: stage no.$STAGE is skipped with command line arg"
			printf "%-72b" "Skipping tests at ${YELLOW}stage no.$STAGE${NC}"
            echo -e "----------- STAGE no.${STAGE} -----------\n${SKIPPED}\n" >> $TESTLOG

        fi
    done
    return $ISSKIPPED
}

export PHEIXTESTENGINE=1

# for STAGE in {$STARTSTAGE..$ENDSTAGE}
for (( STAGE=$STARTSTAGE; STAGE<=$ENDSTAGE; STAGE++ ))
do
	if [ $STAGE -eq -1 ] && stage_is_skipped $STAGE -eq 0
	then
		if [ -d "${PRECOMP}" ]; then
			rm -rf ${PRECOMP}
		else
			echo -e "Skip delete of ${YELLOW}${PRECOMP}${NC} folder: not existed"
		fi
		printf "%-72b" "Running ${YELLOW}user.raku${NC} with --mode=test argument"
        check_output "`raku ${WWW}/user.raku --mode=test && echo ${SUCCESSTOKEN}`" $STAGE
	elif [ $STAGE -eq 0 ] && stage_is_skipped $STAGE -eq 0
	then
		#echo -e "Running ${YELLOW}Pheix::Addons::November::CGI${NC} tests no.1"
		printf "%-72b" "Running ${YELLOW}Pheix::Addons::November::CGI${NC} tests no.1"
		check_output "`./t/cgi/cgi_post_test.sh && echo ${SUCCESSTOKEN}`" $STAGE
		if [ $? -eq 0 ]
		then
			echo -e "[ ${GREEN}OK${NC} ]"
			printf "%-72b" "Running ${YELLOW}Pheix::Addons::November::CGI${NC} tests no.2"
			check_output "`raku ./t/00-november.t && echo ${SUCCESSTOKEN}`" $STAGE
		else
			cat ${HASH} 2> /dev/null
		fi
	elif [ $STAGE -eq 1 ] && stage_is_skipped $STAGE -eq 0
	then
		printf "%-72b" "Running ${YELLOW}Pheix::Model::JSON${NC} tests"
		check_output "`raku ./t/01-json.t ${SCHASH} && echo ${SUCCESSTOKEN}`" $STAGE
	elif [ $STAGE -eq 2 ] && stage_is_skipped $STAGE -eq 0
	then
		printf "%-72b" "Running ${YELLOW}Pheix::Model::Database::Access${NC} filechain tests"
		check_output "`raku ./t/02-access-filechain.t && echo ${SUCCESSTOKEN}`" $STAGE
	elif [ $STAGE -eq 3 ] && stage_is_skipped $STAGE -eq 0
	then
		printf "%-72b" "Running ${YELLOW}Pheix::Model::Database::Access${NC} unified tests"
		check_output "`raku ./t/03-access-unified.t && echo ${SUCCESSTOKEN}`" $STAGE
	elif [ $STAGE -eq 4 ] && stage_is_skipped $STAGE -eq 0
	then
		printf "%-72b" "Running ${YELLOW}Pheix::Model::Database::Blockchain${NC} common tests"
		check_output "`raku ./t/04-blockchain-common.t && echo ${SUCCESSTOKEN}`" $STAGE
	elif [ $STAGE -eq 5 ] && stage_is_skipped $STAGE -eq 0
	then
		printf "%-72b" "Running ${YELLOW}Pheix::Model::Database::Blockchain${NC} heavy test"
		check_output "`raku ./t/05-blockchain-heavy.t && echo ${SUCCESSTOKEN}`" $STAGE
	elif [ $STAGE -eq 6 ] && stage_is_skipped $STAGE -eq 0
	then
		printf "%-72b" "Running ${YELLOW}Pheix::Model::Database::Blockchain${NC} comp test"
		check_output "`raku ./t/06-blockchain-comp.t && echo ${SUCCESSTOKEN}`" $STAGE
	elif [ $STAGE -eq 7 ] && stage_is_skipped $STAGE -eq 0
	then
		printf "%-72b" "Running ${YELLOW}Pheix::Model::Database::Blockchain${NC} mocked write test"
		check_output "`raku ./t/07-blockchain-write.t && echo ${SUCCESSTOKEN}`" $STAGE
	elif [ $STAGE -eq 8 ] && stage_is_skipped $STAGE -eq 0
	then
		printf "%-72b" "Running ${YELLOW}Pheix::Datepack${NC} tests"
		check_output "`raku ./t/08-datepack.t && echo ${SUCCESSTOKEN}`" $STAGE
	elif [ $STAGE -eq 9 ] && stage_is_skipped $STAGE -eq 0
	then
		printf "%-72b" "Running ${YELLOW}Pheix::View::Debug${NC} tests"
		check_output "`raku ./t/09-debug.t && echo ${SUCCESSTOKEN}`" $STAGE
	elif [ $STAGE -eq 10 ] && stage_is_skipped $STAGE -eq 0
	then
		printf "%-72b" "Running ${YELLOW}Pheix::View::Web::Headers${NC} tests"
		check_output "`raku ./t/10-headers.t && echo ${SUCCESSTOKEN}`" $STAGE
	elif [ $STAGE -eq 11 ] && stage_is_skipped $STAGE -eq 0
	then
		printf "%-72b" "Running ${YELLOW}Pheix::Model::Version${NC} tests"
		check_output "`raku ./t/11-version.t ${GITVER} ${CURRVER} && echo ${SUCCESSTOKEN}`" $STAGE
	elif [ $STAGE -eq 12 ] && stage_is_skipped $STAGE -eq 0
	then
		printf "%-72b" "Running ${YELLOW}Pheix::Utils${NC} tests"
		check_output "`raku ./t/12-utils.t && echo ${SUCCESSTOKEN}`" $STAGE
	elif [ $STAGE -eq 13 ] && stage_is_skipped $STAGE -eq 0
	then
		printf "%-72b" "Running ${YELLOW}Pheix::View::Template${NC} tests"
		check_output "`raku ./t/13-template.t && echo ${SUCCESSTOKEN}`" $STAGE
	elif [ $STAGE -eq 14 ] && stage_is_skipped $STAGE -eq 0
	then
		printf "%-72b" "Running ${YELLOW}Pheix::Controller::Stats${NC} tests"
		check_output "`raku ./t/14-stats.t && echo ${SUCCESSTOKEN}`" $STAGE
	elif [ $STAGE -eq 15 ] &&stage_is_skipped $STAGE -eq 0
	then
		printf "%-72b" "Running ${YELLOW}Pheix::Model::Resources::En${NC} tests"
		check_output "`raku ./t/15-resources-en.t && echo ${SUCCESSTOKEN}`" $STAGE
	elif [ $STAGE -eq 16 ] && stage_is_skipped $STAGE -eq 0
	then
		printf "%-72b" "Running ${YELLOW}Pheix::View::HTML::Markup${NC} tests"
		check_output "`raku ./t/16-markup.t && echo ${SUCCESSTOKEN}`" $STAGE
	elif [ $STAGE -eq 17 ] && stage_is_skipped $STAGE -eq 0
	then
		printf "%-72b" "Running ${YELLOW}Pheix::View::Web::Headers${NC} proto_sn test no.1"
		export SERVER_NAME=https://foo.bar
		check_output "`raku ./t/17-headers-proto-sn.t && echo ${SUCCESSTOKEN}`" $STAGE
		if [ $? -eq 0 ]
		then
			echo -e "[ ${GREEN}OK${NC} ]"
			printf "%-72b" "Running ${YELLOW}Pheix::View::Web::Headers${NC} proto_sn test no.2"
			export SERVER_NAME=https://foo.bar/
			check_output "`raku ./t/17-headers-proto-sn.t && echo ${SUCCESSTOKEN}`" $STAGE
			if [ $? -eq 0 ]
			then
				echo -e "[ ${GREEN}OK${NC} ]"
				printf "%-72b" "Running ${YELLOW}Pheix::View::Web::Headers${NC} proto_sn test no.3"
				export SERVER_NAME=//foo.bar/
				check_output "`raku ./t/17-headers-proto-sn.t && echo ${SUCCESSTOKEN}`" $STAGE
			else
				cat ${HASH} 2> /dev/null
			fi
		else
			cat ${HASH} 2> /dev/null
		fi
	elif [ $STAGE -eq 18 ] && stage_is_skipped $STAGE -eq 0
	then
		printf "%-72b" "Running ${YELLOW}Pheix::View::Web::Headers${NC} proto test no.1"
		check_output "`raku ./t/18-headers-proto.t && echo ${SUCCESSTOKEN}`" $STAGE
		if [ $? -eq 0 ]
		then
			echo -e "[ ${GREEN}OK${NC} ]"
			printf "%-72b" "Running ${YELLOW}Pheix::View::Web::Headers${NC} proto test no.2"
			export HTTP_REFERER=//foo.bar
			check_output "`raku ./t/18-headers-proto.t && echo ${SUCCESSTOKEN}`" $STAGE
			if [ $? -eq 0 ]
			then
				echo -e "[ ${GREEN}OK${NC} ]"
				printf "%-72b" "Running ${YELLOW}Pheix::View::Web::Headers${NC} proto test no.3"
				export HTTP_REFERER=https://foo.bar
				check_output "`raku ./t/18-headers-proto.t && echo ${SUCCESSTOKEN}`" $STAGE
			else
				cat ${HASH} 2> /dev/null
			fi
		else
			cat ${HASH} 2> /dev/null
		fi
	elif [ $STAGE -eq 19 ] && stage_is_skipped $STAGE -eq 0
	then
		printf "%-72b" "Running ${YELLOW}Pheix::View::Web::Cookie${NC} tests"
		check_output "`raku ./t/19-cookie.t && echo ${SUCCESSTOKEN}`" $STAGE
	elif [ $STAGE -eq 20 ] && stage_is_skipped $STAGE -eq 0
	then
		printf "%-72b" "Running ${YELLOW}Pheix::View::Pages${NC} tests"
		check_output "`raku ./t/20-pages.t && echo ${SUCCESSTOKEN}`" $STAGE
	elif [ $STAGE -eq 21 ] && stage_is_skipped $STAGE -eq 0
	then
		printf "%-72b" "Running ${YELLOW}Pheix::Controller::Basic${NC} tests"
		check_output "`raku ./t/21-controller-basic.t && echo ${SUCCESSTOKEN}`" $STAGE
	elif [ $STAGE -eq 22 ] && stage_is_skipped $STAGE -eq 0
	then
	    printf "%-72b" "Running ${YELLOW}Pheix::Controller::API${NC} tests"
	    check_output "`raku ./t/22-controller-api.t && echo ${SUCCESSTOKEN}`" $STAGE
	elif [ $STAGE -eq 23 ] && stage_is_skipped $STAGE -eq 0
	then
	    printf "%-72b" "Running ${YELLOW}Pheix::App${NC} tests"
	    check_output "`raku ./t/23-app.t && echo ${SUCCESSTOKEN}`" $STAGE
	elif [ $STAGE -eq 24 ] && stage_is_skipped $STAGE -eq 0
	then
	    printf "%-72b" "Running ${YELLOW}Pheix::Model::Route${NC} tests"
	    check_output "`raku ./t/24-route.t && echo ${SUCCESSTOKEN}`" $STAGE
	elif [ $STAGE -eq 25 ] && stage_is_skipped $STAGE -eq 0
	then
		printf "%-72b" "Running ${YELLOW}Pheix::Model::Database::Compression${NC} tests"
		check_output "`raku ./t/25-compression.t && echo ${SUCCESSTOKEN}`" $STAGE
	elif [ $STAGE -eq 26 ] && stage_is_skipped $STAGE -eq 0
	then
		printf "%-72b" "Running ${YELLOW}Pheix::Addons::Embedded::User${NC} tests"
		check_output "`raku ./t/26-addons-embeddeduser.t && echo ${SUCCESSTOKEN}`" $STAGE
    elif [ $STAGE -eq 27 ] && stage_is_skipped $STAGE -eq 0
	then
		printf "%-72b" "Running ${YELLOW}Pheix::Controller::Blockchain::Signer${NC} tests"
		check_output "`raku ./t/27-naive-signer.t && echo ${SUCCESSTOKEN}`" $STAGE
    elif [ $STAGE -eq 28 ] && stage_is_skipped $STAGE -eq 0
	then
		printf "%-72b" "Running ${YELLOW}Pheix::Model::Database::Blockchain${NC} deploy tests"
		check_output "`raku ./t/28-deploy-smart-contract.t && echo ${SUCCESSTOKEN}`" $STAGE
	fi

	if [ $? -eq 0 ]
	then
		if [ -z "$SKIPPED" ]; then
			let "ITERATOR = ($ITERATOR + 1)"
			let "COVERAGE = ($ITERATOR)*100/($ENDSTAGE+2)"
			echo -e "[ ${GREEN}$COVERAGE% covered${NC} ]"
		else
			echo -e "[ ${RED}SKIP${NC} ]"
		fi
		# echo -e "[ ${GREEN}all tests passed at stage ${STAGE} ($COVERAGE% covered)${NC} ]"
		unset HTTP_REFERER
		unset SERVER_NAME
	else
		echo -e "[ ${RED}error at stage ${STAGE}${NC} ]"
		unset HTTP_REFERER
		unset SERVER_NAME
		unset PHEIXTESTENGINE
		exit -1;
	fi
done

unset PHEIXTESTENGINE

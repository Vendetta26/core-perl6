unit class Pheix::Model::Database::Blockchain;

use Net::Ethereum;
use Pheix::Model::Database::Filechain;
use Pheix::Model::Database::Compression;
use Pheix::Model::Database::Blockchain::SendTx;

has Str  $.table;
has Str  $.apiurl;
has Str  $.ethacc is rw;
has Int  $.gasqty = 20000000;
has Int  $.gascap = 15;
has Bool $.test   = False;
has Int  $.gaslim is default(0) is rw;
has Str  $.sctx   is default('0x' ~ q{0} x 64) is rw;
has Str  $.scaddr is default('0x' ~ q{0} x 40) is rw;
has UInt $.nonce  is default(0) is rw;
has Any  $.ethobj is default(Nil) is rw;
has Any  $.cmpobj is default(Nil) is rw;
has Any  $.sgnobj is default(Nil) is rw;
has Str  $.evsign is default('PheixAccess(bytes32,uint8,uint256)') is rw;
has Bool $.comp   is default(False) is rw;
has Bool $.debug  is default(False) is rw;
has Bool $.nounlk is default(False) is rw;
has Str  $.fabi;
has      $.fields;
has      $.sgnlog is default([]) is rw;;

submethod BUILD(
    Str  :$abi!,
    Str  :$apiurl!,
    Str  :$sctx!,
    Str  :$table!,
    List :$fields!,
    Str  :$account,
    Str  :$unlockpwd,
    Str  :$fabi,
    Bool :$test,
    Any  :$utls,
    Bool :$debug,
) returns Bool {
    my Bool $rc = False;
    $!sctx      = $sctx;
    $!table     = $table;
    $!fields    = $fields;
    $!fabi      = $fabi // q{};
    $!test      = $test // False;
    $!debug     = $debug // False;

    if ($utls.^name eq 'Pheix::Utils') {
        $!cmpobj = Pheix::Model::Database::Compression.new(
            :dictsize($utls.lzwsz)
        );
    }
    else {
        $!cmpobj = Pheix::Model::Database::Compression.new;
    }

    if ($apiurl) {
        my $eth_acc = q{};

        $!ethobj = Net::Ethereum.new(
             :api_url($apiurl),
             :abi($abi),
             :keepalive(True),
             :unlockpwd($unlockpwd),
             :debug($debug)
        );

        if self.is_node_active {
            $eth_acc = $!ethobj.eth_accounts[0] if $!ethobj.eth_accounts;

            my Int $bgs =
                ($!ethobj.eth_getBlockByNumber('latest')<gasLimit> // 0).Int;

            $!gaslim = $bgs - ($bgs * 0.05).Int;
        }

        $!ethacc = $account && $account ne q{} ?? $account !! $eth_acc;
        $!apiurl = $apiurl;
        $rc      = True;
    }

    return $rc;
}

method !read_blockchain(Str :$method!, Hash :$data) returns Hash {
    my %rc;

    my Hash $dt = $data if ($data && $data.pairs);

    if (
        $method &&
        $!ethobj &&
        !( $!scaddr ~~ m:i/^ 0x<[0]>**40 $/ )
    ) {
        %rc = $!ethobj.contract_method_call($method, $dt);
    }

    return %rc;
}

method write_blockchain(
    Str  :$method!,
    Hash :$data,
    Bool :$waittx!,
) returns Hash {
    sprintf(
        "\nDEBUG1: method:%s\n\tdata:%s\n\twaittx:%s",
        $method.gist,
        $data.gist,
        $waittx.gist
    ).say if $!debug;

    my %ret =
        status => False,
        txhash => ('0x' ~ q{0} x 64),
        error => q{}
    ;

    my Hash $dt = $data if ($data && $data.pairs);

    if (
        $method &&
        $!ethobj &&
        !( $!scaddr ~~ m:i/^ 0x<[0]>**40 $/ )
    ) {
        my Str $tx;
        my UInt $txgas = 0;
        my UInt $puretxgas = 0;

        try {
            $txgas = self.ethobj.contract_method_call_estimate_gas(
                $method,
                $data,
            ) // 0;

            CATCH {
                default {
                    %ret<error> = .message;
                }
            };
        }

        $puretxgas = $txgas;

        $txgas += self.gas_update(:gas($puretxgas));

        sprintf("DEBUG3: txgas corrected: %d/%d", $txgas, $!gaslim).say if $!debug;

        if $txgas > 0 && $txgas < $!gaslim {
            $tx = Pheix::Model::Database::Blockchain::SendTx
                .new(
                    :signerobj($!sgnobj),
                    :targetobj(self)
                )
                .send_signed_tx(
                    :bcobj(self),
                    :method($method),
                    :data($dt),
                    :txgas($txgas),
                    :waittx($waittx)
                );
        }

        sprintf(
            "DEBUG4: used gas by %s method with data <%s>: %d",
            $method,
            $data<rowdata> // q{},
            $txgas // $!gasqty
        ).say if $!debug;

        if ($tx && $tx ~~ m:i/^ 0x<xdigit>**64 $/) {
            ($method ~ ':'  ~ $waittx).say if $!debug;

            my @res =
                 $!ethobj.wait_for_transaction(
                     :hashes(@($tx)),
                 ) if $waittx;

            %ret<status> = True if (@res.elems == 1 || !$waittx);
            %ret<txhash> = $tx;

            sprintf("DEBUG5: %s method trx: %s", $method,  %ret<txhash>).say
                if $!debug;
        }
        else {
            if $txgas > 0 && $txgas < $!gaslim {
                %ret<error> = sprintf("invalid transaction hash <%s>", $tx);
            }
            else {
                %ret<error> = sprintf("recalculated gas exceed: %d/%d/%d", $puretxgas, $txgas, $!gaslim)
                    if %ret<error> eq q{};
            }
        }
    }

    return %ret;
}

method !read_integer(Str :$method!, Str :$rcname!, Hash :$data) returns Int {
    my Int $rv   = -1;

    my %callhash =
        self!read_blockchain(
            :method($method),
            :data($data),
        );

    if ((%callhash) && (%callhash{$rcname}:exists)) {
        $rv = %callhash{$rcname}.Int;
    }

    return $rv;
}

method !read_unsigned_integer(
    Str  :$method!,
    Str  :$rcname!,
    Hash :$data
) returns UInt {
    my UInt $rv  = 0;

    my %callhash =
        self!read_blockchain(
            :method($method),
            :data($data),
        );

    if ((%callhash) && (%callhash{$rcname}:exists)) {
        $rv = %callhash{$rcname}.Int;
    }

    return $rv;
}

method !read_string(Str :$method!, Str :$rcname!, Hash :$data) returns Str {
    my Str $rs;

    my %callhash =
        self!read_blockchain(
            :method($method),
            :data($data),
        );

    if ((%callhash) && (%callhash{$rcname}:exists)) {
        $rs = %callhash{$rcname}.Str;
    }

    return $rs;
}

method !data_compress(Str :$data!, Bool :$compress) returns Str {
    my Str  $ret = $data;
    my Bool $cmp = $compress // $!comp;

    if $cmp {
        if $!cmpobj {
            my int $r;

            $ret = $!cmpobj.compress(:data($data));
            $r   = $!cmpobj.get_ratio;

            sprintf(
                "\t compression ratio %3d%% - %d/%d",
                $r,
                $!cmpobj.get_bytes(:data($ret)),
                $!cmpobj.get_bytes(:data($data))
            ).say if $!debug;
        }
    }

    return $ret;
}

method !data_decompress(Str :$data!, Bool :$compress) returns Str {
    my Str  $ret = $data;
    my Bool $cmp = $compress // $!comp;

    if $cmp {
        if $!cmpobj {
            $ret = $!cmpobj.decompress(:data($data));
        }
    }

    return $ret;
}

method get_logs(UInt :$from, UInt :$to, Str :$address) returns List {
    my List $ret;

    if $!ethobj {
        my $tsha   = $!ethobj.web3_sha3($!ethobj.string2hex($!evsign));
        my %filter = $!ethobj.pack_filter_params(
            :fromblock($from),
            :toblock($to),
            :address($address),
            :topics([$tsha])
        );

        $ret = $!ethobj.eth_getLogs(%filter);
    }

    return $ret;
}

method get_path returns Str {
    return $!fabi // q{};
}

method get_modify_time(Str :$t) returns Instant {
    my Instant $ret;
    my UInt $mtime = 0;

    if $t {
        $mtime = self!read_unsigned_integer(
            :method('getTableModTime'),
            :rcname('tabmtime'),
            :data(%(tabname => $t // $!table))
        );
    }
    else {
        $mtime = self!read_unsigned_integer(
            :method('getGlobalModTime'),
            :rcname('globmtime')
        );
    }

    # https://en.wikipedia.org/wiki/Leap_second#History -10 secs to get 0
    # https://github.com/Raku/old-issue-tracker/issues/5805

    return $mtime > 0 ?? Instant.from-posix($mtime) !! Instant.from-posix(-10);
}

method get_smartcontract_ver returns Str {
    return self!read_string(
        :method('getVersion'),
        :rcname('version')
    );
}

method wait_for_transactions(:@hashes!) returns Bool {
    my Bool $rc = False;

    if @hashes {
        my @res = $!ethobj.wait_for_transaction(:hashes(@hashes));
        if @res.elems == @hashes.elems {
            $rc = True;
        }
    }

    return $rc;
}

method is_node_active returns Bool {
    my Bool $rc = False;

    if ($!ethobj) {
        my %h = $!ethobj.node_ping;
        if (%h<retcode>:exists) {
            $rc = True if (%h<retcode> == 0);
        }
    }

    return $rc;
}

method set_contract_addr returns Bool {
    my Bool $rc = False;

    if ($!ethobj && $!sctx ne q{} && !($!sctx ~~ m:i/^ 0x<[0]>**64 $/)) {
        $!scaddr = $!ethobj.retrieve_contract($!sctx);
        $rc = True;
    }

    return $rc;
}

method is_tab_compressed(Str :$t) returns Bool {
    return self!read_integer(
        :method('isTabCompressed'),
        :rcname('comp'),
        :data(%(tabname => $t // $!table)),
    ).Bool;
}

method is_row_compressed(Str :$t, UInt :$id!) returns Bool {
    my Bool $rc = False;

    if $id > 0 {
        $rc = self!read_integer(
            :method('isRecCompressed'),
            :rcname('comp'),
            :data(%(tabname => $t // $!table, rowid => $id)),
        ).Bool;
    }

    return $rc;
}

method get_id_byindex(Str :$t, Int :$index!) returns Int {
    my Int $rv = -1;

    if ($index >= 0) {
        $rv = self!read_integer(
            :method('getIdByIndex'),
            :rcname('rowid'),
            :data(%(tabname => $t // $!table, index => $index)),
        );
    }

    return $rv;
}

method get_data_byindex(Str :$t, Int :$index!, Bool :$comp) returns Str {
    my Str $rs;

    if ($index >= 0) {
        my Str $s = self!read_string(
            :method('getDataByIndex'),
            :rcname('data'),
            :data(%(tabname => $t // $!table, index => $index)),
        );

        $rs = self!data_decompress(
            :data($s),
            :compress($comp)
        ) if ($s && $s.chars);
    }

    return $rs;
}

method get_tabname_byindex(Str :$t, Int :$index!) returns Str {
    my Str $rs;

    if ($index >= 0) {
        $rs = self!read_string(
            :method('getNameByIndex'),
            :rcname('name'),
            :data(%(tabname => $t // $!table, index => $index)),
        );
    }

    return $rs;
}

method get_max_id(Str :$t) returns UInt {
    return self!read_unsigned_integer(
        :method('getMaxId'),
        :rcname('rowid'),
        :data(%(tabname => $t // $!table )),
    );
}

method count_rows(Str :$t) returns UInt {
    return self!read_unsigned_integer(
        :method('countRows'),
        :rcname('count'),
        :data(%(tabname => $t // $!table)),
    );
}

method count_tables returns UInt {
    return self!read_unsigned_integer(:method('countTables'), :rcname('count'));
}


method table_index(Str :$t) returns Int {
    return self!read_integer(
        :method('getTableIndex'),
        :rcname('index'),
        :data(%(tabname => $t // $!table)),
    );
}

method table_exists(Str :$t) returns Bool {
    my Bool $rc  = False;

    my %callhash =
        self!read_blockchain(
            :method('tableExists'),
            :data({tabname => $t // $!table})
        );

    if ((%callhash) && (%callhash<success>:exists)) {
        $rc = True if (
            (%callhash<success> ~~ m:i/ true /) ||
            (%callhash<success> == 1)
        );
    }

    return $rc;
}

method id_exists(Str :$t, UInt :$id!) returns Bool {
    my Bool $rc  = False;

    my %callhash =
        self!read_blockchain(
            :method('idExists'),
            :data(%(tabname => $t // $!table, rowid => $id))
        );

    if ( (%callhash) && (%callhash<success>:exists) ) {
        $rc = True if (
            (%callhash<success> ~~ m:i/ true /) ||
            (%callhash<success> == 1)
        );
    }

    return $rc;
}

method table_debug(Bool :$waittx) returns Hash {
    return self.write_blockchain(:method('init'), :waittx($waittx // True));
}

method get_table_fields(Str :$t) returns Str {
    return self!read_string(
        :method('getFields'),
        :rcname('fields'),
        :data(%(tabname => $t // $!table)),
    );
}

method table_create(Str :$t, Bool :$waittx, Bool :$comp) returns Hash {
    return self.write_blockchain(
        :method('newTable'),
        :data(
            %(
                tabname => $t // $!table,
                fields  => '# ' ~ $!fields.join(q{;}),
                comp    => $comp // $!comp
            )
        ),
        :waittx($waittx // True),
    );
}

method row_insert(
    Str  :$t,
    UInt :$id,
    Hash :$data!,
    Bool :$waittx,
    Bool :$comp
) returns Hash {
    my %ret  = status => False, txhash => ('0x' ~ q{0} x 64);

    my @cols = $!fields.map({
        $data{$_} ?? $data{$_}.Str !! q{}
    });

    my UInt $rowid   = $id // 0;
    my Str  $rowdata = @cols.join(q{|});

    %ret = self.write_blockchain(
        :method('insert'),
        :data(
            %(
                tabname => $t // $!table,
                rowdata => self!data_compress(
                    :data(@cols.join(q{|})),
                    :compress($comp)
                ),
                id      => $rowid,
                comp    => $comp // $!comp,
            )
        ),
        :waittx($waittx // True),
    );

    return %ret;
}

method row_set(
    Str  :$t,
    Hash :$data!,
    Hash :$clause!
) returns Hash {
    my UInt $id = $clause<id>:exists ?? $clause<id> !! 0;

    return self.update(:t($t), :id($id), :data($data));
}

method update(
    Str  :$t,
    UInt :$id!,
    Hash :$data!,
    Bool :$waittx,
    Bool :$comp
) returns Hash {
    my %ret  = status => False, txhash => ('0x' ~ q{0} x 64);
    my @cols = $!fields.map({
        $data{$_} ?? $data{$_}.Str !! q{}
    });

    if ($id > 0) {
        %ret = self.write_blockchain(
            :method('set'),
            :data(
                %(
                    tabname => $t // $!table,
                    rowid   => $id,
                    rowdata => self!data_compress(
                        :data(@cols.join(q{|})),
                        :compress($comp)
                    ),
                    comp    => $comp // $!comp,
                )
            ),
            :waittx($waittx // True),
        );
    }

    return %ret;
}

method row_get(Str :$t, Hash :$clause!, Bool :$comp) returns List {
    my @rows;
    my UInt $id = $clause<id>:exists ?? $clause<id> !! 0;

    @rows.push(
        self.select(
            :t($t // $!table),
            :id($id),
            :comp($comp // False)
        )
    );

    return @rows;
}

method select(Str :$t, UInt :$id!, Bool :$comp) returns Hash {
    my %ret;
    my Str $rs;

    if ($id >= 0) {
        my %callhash =
            self!read_blockchain(
                :method('select'),
                :data(%(tabname => $t // $!table, rowid => $id))
            );

        if (%callhash) && (%callhash<data>:exists) {
            $rs = self!data_decompress(
                :data(%callhash<data>),
                :compress($comp)
            ) if (
                (%callhash<data>) &&
                (%callhash<data>.chars)
            );
        }
    }

    if ($rs) {
        %ret = (
            Pheix::Model::Database::Filechain.
                new(:fields($!fields)).
                    get_from_array(%(Nil), @($rs), True)
        )[0];
    }

    return %ret;
}

method row_remove(Str :$t, Hash :$clause!) returns Bool {
    my UInt $id = $clause<id>:exists ?? $clause<id> !! 0;
    my %ret     = self.delete(:t($t), :id($id));

    return %ret<status>;
}

method delete(Str :$t, UInt :$id!, Bool :$waittx) returns Hash {
    return self.write_blockchain(
        :method('remove'),
        :data(
            %(
                tabname => $t // $!table,
                rowid   => $id
            )
        ),
        :waittx($waittx // True),
    );
}

method set_tab_cmp(
    Str  :$t,
    Bool :$comp,
    Bool :$waittx
) returns Hash {
    return self.write_blockchain(
        :method('updateTabCompression'),
        :data(
            %(
                tabname => $t // $!table,
                comp    => $comp // $!comp,
            )
        ),
        :waittx($waittx // True)
    );
}

method set_row_cmp(
    Str  :$t,
    UInt :$id!,
    Bool :$comp,
    Bool :$waittx
) returns Hash {
    my %ret  = status => False, txhash => ('0x' ~ q{0} x 64);

    if ($id > 0) {
        %ret = self.write_blockchain(
            :method('updateRecCompression'),
            :data(
                %(
                    tabname => $t // $!table,
                    rowid   => $id,
                    comp    => $comp // $!comp,
                )
            ),
            :waittx($waittx // True)
        );
    }

    return %ret;
}

method set_table_fields(Str :$t, :@fields, Bool :$waittx) returns Hash {
    return self.write_blockchain(
        :method('setFields'),
        :data(
            %(
                tabname => $t // $!table,
                fields  => '# ' ~ (@fields // $!fields).join(q{;}),
            )
        ),
        :waittx($waittx // True)
    );
}

method unlock_account(Str :$acc) returns Bool {
    return
        $!nounlk ??
            True !!
                $!ethobj.personal_unlockAccount(:account($acc // $!ethacc));
}

method update_tab_mtime(
    Str  :$t,
    Bool :$waittx
) returns Hash {
    return self.write_blockchain(
        :method('updateModTimes'),
        :data(%(tabname => $t // $!table)),
        :waittx($waittx // True)
    );
}

method deploy_precompiled_smartcontract(
    Hash :$constructor_params = {}
) returns Hash {
    return $!ethobj.compile_and_deploy_contract(
        :contract_path($!fabi),
        :compile_output_path($!fabi.IO.dirname),
        :skipcompile(True),
        :constructor_params(%$constructor_params)
    );
}

method gas_update(UInt :$gas) returns UInt {
    my $percentage = ($gas > $!gasqty/5) ?? $!gascap !! ($!gascap * 3);
    my $gasupdate  = $percentage * ($gas/100).floor;

    sprintf(
        "DEBUG2: txgas:%s, gasqty:%s, gascap:%s, percent:%s, gasupd: %s",
        $gas.gist,
        $!gasqty.gist,
        $!gascap.gist,
        $percentage,
        $gasupdate
    ).say if $!debug;

    return $gasupdate;
}

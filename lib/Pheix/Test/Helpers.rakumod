unit class Pheix::Test::Helpers;

has Str  $!dbpath    = 'conf/system';
has UInt $.goerlipid = 1598179320;
has UInt $.remarkpid = 1628445000;
has @!stats;

method get_addon_sitemap_from_fs(:$addon!) returns List {
    my @sitemap;

    my %database = ($addon.jsonobj.get_entire_config)<database>;

    for %database.keys -> $record {
        my $pid    = %database{$record}<id>;
        my $seouri = %database{$record}<seouri>;

        my $d = DateTime.new(
            sprintf("%s/%s/%s.tnk", $!dbpath, $addon.get_name.lc, $pid)
                .IO
                .modified || now
        );

        my $lastmod = sprintf("%04d-%02d-%02d", $d.year, $d.month, $d.day);

        my $path =
            %database{$record}<allowpage> == 1 ??
                $addon.get_name.lc ~ '/page/' !!
                    $addon.get_name.lc ~ q{/};

        my $page =
            $seouri && $seouri.chars ??
                $seouri !!
                    ($addon.allowid ?? $pid !! Str);

        if $page {
            next if $pid == $!goerlipid;

            @sitemap.push(
                {
                    loc     => $path ~ $page,
                    lastmod => $lastmod,
                }
            );
        }
    }

    @sitemap;
}

method time_stats(Str :$module!) returns UInt {
    my $stime;

    "***INFO: subtests:".say;

    for @!stats -> %h {
        printf("%16s : %10f secs\n", %h<descr>, %h<time>);
        $stime += %h<time>;
    }

    printf(
        "***INFO: total for %s unit tests <%.3f> secs\n",
        $module,
        $stime
    );

    1;
}

method get_code_time(Str :$descr!, :$coderef!) returns Duration {
    my $mtime = { :descr($descr), :start(now), :time(0) };

    {
        $coderef();
    }

    $mtime<time> = now - $mtime<start>;
    push(@!stats, $mtime);

    $mtime<time>;
}

unit class Pheix::App;

use Router::Right;

use Pheix::Addons;
use Pheix::Controller::Basic;
use Pheix::Model::Route;

use FastCGI::NativeCall::Async;

has UInt $!tick is default(1);
has Bool $.test is default(False);

has %.addons                   = Pheix::Addons.new.get_addons;
has Pheix::Model::Route $.robj = Pheix::Model::Route.new(:test($!test));

has Pheix::Controller::Basic $.ctrl = Pheix::Controller::Basic.new(
    :apirobj(
        $!robj.import_addon_routes(
            :prefix('/api'),
            :router(
                $!robj.basic_routes_with_handler(
                    :handler_class('Pheix::Controller::API')
                )
            ),
            :addons(%!addons)
        )
    ),
    :test($!test),
    :addons(%!addons),
);

has Router::Right $.route =
    $!robj.import_addon_routes(
        :prefix(Str),
        :router(
            $!robj.basic_routes_with_handler(
                :handler_class($!ctrl.^name.Str)
            )
        ),
        :addons(%!addons)
    );

has $!fastcgiasync =
    $!test ??
        Promise.start({$!ctrl.sharedobj<fastcgi>}) !!
            FastCGI::NativeCall::Async.new;

method start {
    react {
        whenever $!fastcgiasync -> $fastcgi {
            $!ctrl.set_fastcgi(:$fastcgi) if not $!test;

            %*ENV = $!ctrl.sharedobj<fastcgi>.env;

            if !(
                my %m = $!route.match(
                    %*ENV<REQUEST_URI> || '/index',
                    %*ENV<REQUEST_METHOD> || 'GET',
                )
            ) {
                $!ctrl.error(
                    :tick($!tick),
                    :code($!route.error.UInt),
                    :req(%*ENV<REQUEST_URI>)
                );
            }
            else {
                my $custom_ctrl = $!ctrl;

                if %m<controller> ne $!ctrl.^name {
                    if %!addons{%m<controller>}:exists {
                        $custom_ctrl =
                            %!addons{%m<controller>}<objct>
                                .get(:ctrl($!ctrl));
                    }
                }

                $custom_ctrl."%m<action>"(:tick($!tick), :match(%m));
                spurt "/tmp/log.txt", %m.gist;

                CATCH {
                    default {
                        $!ctrl.error(
                            :tick($!tick),
                            :code(400),
                            :req(%m<details><path>),
                            :msg(.message ~ .backtrace)
                        );
                    }
                };
            }

            $!tick++;
        }
    }
}

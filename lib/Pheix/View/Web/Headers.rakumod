unit class Pheix::View::Web::Headers;

use Pheix::Datepack;
use Pheix::Model::JSON;

has $!dpobj = Pheix::Datepack.new(:date(DateTime.now), :unixtime(time));
has $!jsobj = Pheix::Model::JSON.new;
has Str $!crlf  = "\x[0D]\x[0A]";
has Str $!proto = 'http://';
has Str $.addon = 'Pheix';

has %!default_fields =
    Cache-Control => 'no-cache,no-store,max-age=0,must-revalidate',
    Content-Type  => 'text/html; charset=UTF-8',
    Expires       => $!dpobj.get_http_response_date,
    Last-Modified => $!dpobj.get_http_response_date,
    P3P           => 'CP="Pheix does not have a P3P policy."',
    Status        => 200,
;

method header(%fields, @cookies, Bool $compose?) returns Hash {
    my Str $hstr;
    my %h;

    my %f = %!default_fields.clone;

    if %fields {
        for %fields -> (:$key, :$value) {
            next if !$key or !$value;
            if %f{$key}:exists {
                %f{$key} = $value;
            }
            else {
                %f.push: ($key => $value);
            }
        }
    }

    for %f -> (:$key, :$value) {
        next if !$key or !$value;

        %h.push: ($key => $value);

        $hstr ~= $key ~ q{:} ~ $value ~ $!crlf;
    }

    if %h {
        my @cs;
        my $ckey = 'Set-Cookie';

        for @cookies -> $c {
            next if !$c || $c eq q{};

            @cs.push($c);
            $hstr ~= $ckey ~ q{:} ~ $c ~ $!crlf;
        }

        %h{$ckey} = @cs if @cs.elems > 0;

        $hstr ~= $!crlf;
    }

    %h<Composed-Header> = $hstr if $compose;

    %h;
}

method proto returns Str {
    if %*ENV<HTTP_REFERER> {
        if %*ENV<HTTP_REFERER> ~~ m/^ https\:\/\/ /  {
            $!proto = 'https://';
        }
    }

    $!proto;
}

method proto_sn returns Str {
    my Str  $p = $!jsobj.get_setting( $!addon, 'workviaproto', 'value')
                ?? 'https://' !! 'http://';
    my Cool $s = $!jsobj.get_setting( $!addon, 'servername', 'value');
    if !$s {
        if %*ENV<SERVER_NAME> {
            $s = %*ENV<SERVER_NAME>;
        }
        else {
            $s = 'undef';
        }
    }
    if $s ne 'undef' {
        $s ~~ s:g:i/^ (https?\:) ** 0..1 '//' //;
        $s ~~ s:g:i/ '/' ** 1..* $//;
    }

    ( $p ~ $s );
}

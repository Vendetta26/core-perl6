unit class Pheix::View::HTML::Markup;

has $.br = '<br>';

method uni_tag(Str $tag, Cool $class, Str $content) returns Str {
    my Str $ret;
    if $tag {
        if $class {
            $ret = "<$tag class=\"$class\">" ~ $content ~ "</$tag>";
        }
        else {
            $ret = "<$tag>" ~ $content ~ "</$tag>";
        }
    }
    $ret;
}


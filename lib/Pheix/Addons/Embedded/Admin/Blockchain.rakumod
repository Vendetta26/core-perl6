unit class Pheix::Addons::Embedded::Admin::Blockchain;

use Pheix::Model::Database::Access;

has UInt $.txwaisec = 1;
has UInt $.sesstime = 300;
has UInt $.sessseed = 999_000_999;
has      %!gateways = {asc => 'auth-smart-contract', agw => 'auth-node'};

has Pheix::Model::Database::Access
    $!agw = Pheix::Model::Database::Access.new(
        :table(%!gateways<agw>),
        :fields([]),
        :txwaitsec($!txwaisec),
    );

method auth_on_blockchain(Str :$addr!, Str :$pwd!) returns Hash {
    my %ret =
        status  => False,
        message => q{},
        pkey    => q{},
        tx      => '0x' ~ q{0} x 64;

    if $!agw && $!agw.dbswitch == 1 && $addr ~~ m:i/^ 0x<xdigit>**40 $/ {
        try {
            %ret<message> = $!agw.chainobj.ethobj.personal_sign(
                :message($!agw.chainobj.ethobj.string2hex(self.^name)),
                :account($addr),
                :password($pwd)
            );
        }

        if $! {
            %ret<message> = $!.message;
        }
        else {
            my Pheix::Model::Database::Access
                $asc = Pheix::Model::Database::Access.new(
                    :table(%!gateways<asc>),
                    :fields([]),
                    :constrpars({_delta => $!sesstime, _seedmod => $!sessseed}),
                    :txwaitsec($!txwaisec),
                );

            if $asc && $asc.dbswitch == 1 {
                my %h = $asc.
                        chainobj.
                        ethobj.
                        contract_method_call('getPkey', {});

                if %h<publickey> && %h<publickey> ~~ m:i/^ 0x<xdigit>**64 $/ {
                    %ret<status> = True;
                    %ret<pkey>   = %h<publickey>;
                    %ret<tx>     = $asc.chainobj.sctx;
                }
            }
        }
    }

    return %ret;
}

method validate_on_blockchain(Str :$token!) returns Hash {
    my %validate  = status => False;
    my Str $scadr = '0x' ~ q{0} x 40;

    my %trx = $!agw.chainobj.ethobj.eth_getTransactionReceipt($token);

    return %validate unless
        $!agw &&
        $!agw.dbswitch == 1 &&
        $token ~~ m:i/^ 0x<xdigit>**64 $/ &&
        %trx<from> ~~ m:i/^ 0x<xdigit>**40 $/;

    # $!agw.chainobj.sctx   = %trx<transactionHash>;
    $!agw.chainobj.ethacc = %trx<from>;

    if %trx<to> && %trx<to> ~~ m:i/^ 0x<xdigit>**40 $/ {
        $scadr = %trx<to>;
    }
    elsif %trx<contractAddress> && %trx<contractAddress> ~~ m:i/^ 0x<xdigit>**40 $/ {
        $scadr = %trx<contractAddress>;
    }

    if $scadr !~~ m:i/^ 0x<[0]>**40 $/ {
        my %updatetrx;

        $!agw.chainobj.scaddr = $scadr;
        $!agw.chainobj.evsign = 'PheixAuthCode(uint256)';
        $!agw.chainobj.ethobj.contract_id = $scadr;

        my $upd = self.session_details(:trx($token));

        if $upd<expired> {
            %updatetrx = $!agw.chainobj.write_blockchain(
                :method('updateState'),
                :waittx(True)
            );

            $upd = self.session_details(:trx(%updatetrx<txhash>:exists ?? %updatetrx<txhash> !! $token));
        }

        my %h = $!agw.chainobj.ethobj.contract_method_call('getPkey', {});

        %h<publickey>:delete if %h<publickey> ~~ m:i/^ 0x<[0]>**64 $/;

        if %h<publickey> && %h<publickey> ~~ m:i/^ 0x<xdigit>**64 $/ && $upd<delta> > 0 {
            my $logs = $!agw.chainobj.get_logs(:address($!agw.chainobj.scaddr));
            %validate =
                status  => True,
                addr    => %trx<from>,
                session => $upd<delta>,
                pkey    => %h<publickey>,
                tx      => %updatetrx<txhash>:exists ?? %updatetrx<txhash> !! $token
            ;
        }
    }

    return %validate;
}

method session_details(Str :$trx!) returns Hash {
    my $ret  = {expired => False};
    my $logs = $!agw.chainobj.get_logs(:address($!agw.chainobj.scaddr));
    my %trx  = $!agw.chainobj.ethobj.eth_getTransactionReceipt($trx);

    if $logs && $logs.elems > 0 {
        if (($logs.head)<transactionHash>:exists) && (%trx<blockNumber>:exists) {
            my $gentrx  = $!agw.chainobj.ethobj.eth_getTransactionReceipt(($logs.head)<transactionHash>);
            $ret<genbn> = $gentrx<blockNumber>.Int;
            $ret<trxbn> = %trx<blockNumber>.Int;
            $ret<block> = $ret<trxbn> > $ret<genbn> ?? $ret<trxbn> !! $ret<genbn>;

            my $prevblck  = $!agw.chainobj.ethobj.eth_getBlockByNumber($ret<block>);
            my $lastblck = $!agw.chainobj.ethobj.eth_getBlockByNumber('latest');

            $ret<delta> = $!sesstime - ($lastblck<timestamp>.Int - $prevblck<timestamp>.Int);

            if $ret<delta> > 0 && $ret<delta> < 15 {
                $ret<expired> = True;
            }
        }
    }

    return $ret;
}

unit class Pheix::Addons;

use Pheix::Model::JSON;

has Bool             $.test = False;
has Pheix::Model::JSON $!jo = Pheix::Model::JSON.new;

method get_addons returns Hash {
    my %ret;

    my %addonscnf = $!jo.get_all_settings_for_group_member(
        'Pheix',
        'addons',
        'installed'
    );

    for %addonscnf.values -> $v {
        my %addondets;
        try require ::($v);
        die sprintf(
            "Required addon %s is failed at %s\:\:%s",
            $v,
            self.^name,
            &?ROUTINE.name
        ) if ::($v) ~~ Failure;

        %addondets<objct> = ::($v).new;
        %addondets<aname> = %addondets<objct>.get_name;
        %addondets<confr> =
            $!jo.get_all_settings_for_group_member(
                %addondets<objct>.get_name,
                'routing',
                'routes'
            );

        if (%addondets<confr>:exists) && (%addondets<objct>:exists) {
            %ret{$v} = %addondets;
        }
    }

    %ret;
}
